# IDMS Assets

## Webpack


## Documenting
- Documentation should be outlined in the `dist/index.html` file.
- The file can then be viewed at https://shib.oit.duke.edu/idms-assets/dist when deployed (https://shib-idp-test-09.oit.duke.edu/idms-assets/dist/ for test)
- Examples of how to use the code should be provided

## Development
### Style
- run `npm run build` to build the bundle.js file
- universal styles should live in `/src/css/base.css`
- duke specific styles should live in `/src/css/duke.css`
- fonts should live in `src/css/fonts.CSS`
- images should live in the `src/img` directory

###  JS files
- Javascript files live in the `/src/js` directory

### Webpack
- This repository uses webpack to package our assets.
- run `npm run build` to build the bundle.js file


### Current Packaged JS Files
- jQuery
- table Sorter
- font awesome


## Deployment
- This repo needs to be cloned into a local copy of the IDP Subversion Repository

- you can run svn co `svn co svn+ssh://yourNetid@community.oit.duke.edu/srv/svn/idms/shibboleth/shibboleth3/web/trunk/html` to
get a local copy of the repository

- This gets tricky so follow these steps
  1. ensure you are in the `shibboleth/shibboleth3/web/trunk/html` directory and that you see a directory of `idms-assets`
  2. if the .git directory inside `idms-assets` is not present run `rm -rf idms-assets/*; rm -rf idms-assets/.git; rm -rf idms-assets/.gitignore` then `git clone git@idms-git.oit.duke.edu:idms/idms-stylesheets.git idms-assets` otherwise you can just run `git pull` in the idms-assets directory
  4. run `svn add idms-assets/*`
  5. run `svn ci -m  "updating stylesheets" idms-assets/*`

- Well that was exhilarating! However all that did was update the svn repo. Now comes the best part: Updating this on each IDP box 1 at a time!!!!!! (repeat the above steps as the idms user on each box)

- If you're just testing you only need to run subversion on `shib-idp-test-09.oit.duke.edu`, otherwise.....

  - **including suggested bash alias for those tired of typing**
  - `alias shibtest09="ssh yourNetID@shib-idp-test-09.oit.duke.edu"`
  - `alias shibtest10="ssh yourNetID@shib-idp-test-10.oit.duke.edu"`

  - `alias shib09="ssh yourNetID@shib-idp-09.oit.duke.edu"`
  - `alias shib10="ssh yourNetID@shib-idp-10.oit.duke.edu"`
  - `alias shib11="ssh yourNetID@shib-idp-11.oit.duke.edu"`
  - `alias shib12="ssh yourNetID@shib-idp-12.oit.duke.edu"`

  - **may need to be on idms-admin-04 to ssh into**
  - `alias shibnc1="ssh yourNetID@shib-idp-nc1-01.oit.duke.edu"`
  - `alias shibnc2="ssh yourNetID@shib-idp-nc1-02.oit.duke.edu"`
  - `alias atc1="ssh yourNetID@shib-idp-atc-01.oit.duke.edu"`
  - `alias atc2="ssh yourNetID@shib-idp-atc-02.oit.duke.edu"`

  - **DKU**
  - `alias sing03="ssh yourNetID@shib-sing-03.oit.duke.edu"`
  - `alias sing04="ssh yourNetID@shib-sing-04.oit.duke.edu"`

- To verify you should now be able to open a browser and go to https://shib.oit.duke.edu/idms-assets/dist and see files or if testing,
https://shib-idp-test-09.oit.duke.edu/idms-assets/dist
