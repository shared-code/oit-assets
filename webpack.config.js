const path = require('path');
const webpack = require("webpack"); //to access built-in plugins


module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  watch: true,
  plugins: [
      new webpack.ProvidePlugin({
          $: "jquery",
          jQuery: "jquery",
          "window.jQuery": "jquery"
      })
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg)$/,
        use: [
          'file-loader?name=public/images/[name].[ext]',
        ]
      },
      //load gif files with url-loader for tablesorter arrows
      {
        test: /\.(gif)$/,
        use: [
          'url-loader?name=public/images/[name].[ext]'
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'url-loader?name=public/fonts/[name].[ext]'
        ]
      }
    ]
  }
};
