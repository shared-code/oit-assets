import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import './css/base.css';
import './css/duke.css';
import './js/tablesorter/jquery.tablesorter.js'
import Bloodhound from './js/typeahead.js'
window.Bloodhound = Bloodhound;

// Import Only Icons we care about
import fontawesome from '@fortawesome/fontawesome'
import faArrowRight from  '@fortawesome/fontawesome-free-solid/faArrowRight'
import faArrowLeft from  '@fortawesome/fontawesome-free-solid/faArrowLeft'
import faPlus from  '@fortawesome/fontawesome-free-solid/faPlus'
import faTrash from  '@fortawesome/fontawesome-free-solid/faTrash'
import faEdit from  '@fortawesome/fontawesome-free-solid/faEdit'
import faSpinner from  '@fortawesome/fontawesome-free-solid/faSpinner'
import faQuestionCircle from  '@fortawesome/fontawesome-free-solid/faQuestionCircle'
import faCog from  '@fortawesome/fontawesome-free-solid/faCog'
import faExternalLinkSquareAlt from  '@fortawesome/fontawesome-free-solid/faExternalLinkSquareAlt'
import faSyncAlt from  '@fortawesome/fontawesome-free-solid/faSyncAlt'
import faClipboard from  '@fortawesome/fontawesome-free-solid/faClipboard'
import faTimes from  '@fortawesome/fontawesome-free-solid/faTimes'



fontawesome.library.add(faArrowRight)
fontawesome.library.add(faArrowLeft)
fontawesome.library.add(faPlus)
fontawesome.library.add(faTrash)
fontawesome.library.add(faEdit)
fontawesome.library.add(faSpinner)
fontawesome.library.add(faQuestionCircle)
fontawesome.library.add(faCog)
fontawesome.library.add(faExternalLinkSquareAlt)
fontawesome.library.add(faSyncAlt)
fontawesome.library.add(faClipboard)
fontawesome.library.add(faTimes)
